module Helpers
where

import Control.Monad (forM)

import Data.Int(Int64)
import qualified Data.ByteString.Lazy as BS

import System.FilePath ((</>))
import System.Directory

areFilesBig :: Int64 -> [BS.ByteString] -> Bool
areFilesBig limit files = any  ( (limit <) . BS.length ) files

getDirSize :: FilePath -> IO Integer
getDirSize rootPath = do
  allFiles <- listDirectory rootPath
  sizes <- forM allFiles $ (\name -> do
    let size = 0
        path = rootPath </> name
    isDir <- doesDirectoryExist path
    if isDir
      then
        return (size+) <*> getDirSize path
      else
        getFileSize path
    )
  return $ sum sizes
