{-# LANGUAGE OverloadedStrings #-}

module Server
where

import qualified Helpers as H

import Web.Scotty
import Data.Monoid
import Data.Maybe
import Data.List (intersect)
import Data.List.Split
import Data.String
import Data.Int(Int64)
import Control.Monad.IO.Class
import Control.Monad (forM_, when, unless)

import Network.Wai.Middleware.RequestLogger
-- import Network.Wai.Middleware.Static
import Network.Wai.Parse
import Network.HTTP.Types

import qualified Data.Text.Lazy as T
import qualified Data.ByteString.Lazy as B
import qualified Data.ByteString.Char8 as BC

import System.FilePath ((</>))
import System.Directory
import System.Posix.Files
import Prelude

type Port = Int

runServer :: Port -> IO ()
runServer port = scotty port $ routes

rootDir::FilePath
rootDir = "/tmp/uploads/"

maxBytesFile::Int64
maxBytesFile = 1000

maxBytesDir::Int64
maxBytesDir = 10000

optParam:: Parsable a => T.Text -> ActionM (Maybe a)
optParam t = (Just <$> param t) `rescue` (\_ -> return Nothing )

hardParam::Parsable a => T.Text -> ActionM a
hardParam t = param t `rescue` (\_ -> do
                                        status status400
                                        json $ "Required param: " `mappend` t `mappend` "is missing"
                                        finish
                               )

getHid :: Maybe T.Text -> String
getHid hid' = T.unpack $ fromMaybe "0" hid'

getRoles :: Maybe T.Text -> [String]
getRoles roles' = read $ T.unpack $ fromMaybe "[\"anonymous\"]" roles' :: [String]


routes :: ScottyM ()
routes = do
  middleware logStdoutDev

  post  "/upload"           uploadCtrl
  get   "/list"             listCtrl
  get   "/file/:filename"   getFileCtrl
  post  "/perms/:filename"  setPermsCtrl
  -- post  "/delete"           deleteCtrl


handleNullSession :: T.Text -> ActionM ()
handleNullSession msg = do
                          status status403
                          json $ msg
                          finish

setPermsCtrl :: ActionM ()
setPermsCtrl = do
  hid'    <- header "X-Hasura-Id"
  hroles' <- header "X-Hasura-Allowed-Roles"

  fileName'  <- hardParam "filename" :: ActionM T.Text
  setRoles'  <- hardParam "setroles" :: ActionM T.Text

  let hid        = getHid hid'
      hroles     = getRoles hroles'
      setRoles   = splitOn "," $ T.unpack setRoles'
      validRoles = setRoles `intersect` hroles
      dirPath    = rootDir </> hid
      fileName   = T.unpack fileName'
      filePath   = rootDir </> hid </> fileName

  fileExists <- liftIO $ doesFileExist filePath

  unless fileExists $ handleNullSession ("Couldn't file " `mappend` fileName' `mappend` " in user dir")

  forM_ validRoles $ (\r -> do
                              let roleDir = rootDir </> r
                              liftIO $ createDirectoryIfMissing True roleDir
                              liftIO $ createSymbolicLink (dirPath </> fileName)  (roleDir </> fileName)
                     )
  json $ validRoles


getFileCtrl :: ActionM ()
getFileCtrl = do
  hid'    <- header "X-Hasura-Id"
  hroles' <- header "X-Hasura-Allowed-Roles"

  fileName <- hardParam "filename"

  let hid    = getHid hid'
      hroles = getRoles hroles'

  filePath <- liftIO $ findFile (map (rootDir </>) (hroles ++ ["anonymous"]) ) fileName

  if isJust filePath
    then
      file $ fromJust filePath
    else do
      status status404
      json $ T.pack "File not found: Use /list to see list of files"


uploadCtrl :: ActionM ()
uploadCtrl = do
  hid'    <- header "X-Hasura-Id"
  hroles' <- header "X-Hasura-Allowed-Roles"

  setRoles'   <- optParam "setroles"

  let hid        = getHid hid'
      hroles     = getRoles hroles'
      setRoles   = splitOn "," $ T.unpack $ fromMaybe "" setRoles'
      validRoles = setRoles `intersect` hroles

  when (hid == "0") (handleNullSession "Unauthorized")

  fs <- files
  let fs' = [ (fieldName, BC.unpack (fileName fi), B.take (maxBytesFile + 1) (fileContent fi) ) | (fieldName,fi) <- fs ]
      dirPath = rootDir </> hid

  dirSize <- liftIO $ H.getDirSize dirPath :: ActionM Integer

  when (dirSize > fromIntegral maxBytesDir) $ do
    status status400
    json $ T.pack $ "Upload limit for user exceeded; Allowed: " ++ show maxBytesDir ++ ", Current: " ++ (show dirSize)
    finish

  when (H.areFilesBig maxBytesFile [fc | (_, _, fc) <- fs']) $ do
    status status400
    json $ T.pack $ "Some files are greater than limit: " ++ show maxBytesFile ++ " bytes"
    finish
  
  liftIO $ createDirectoryIfMissing True dirPath

  -- write the files to disk, so they will be served by the static middleware
  liftIO $ sequence_ [ B.writeFile (dirPath </> fn) fc | (_,fn,fc) <- fs' ]

  --create symlinks in appropriate folders
  forM_ validRoles $ (\r -> do
                              let roleDir = rootDir </> r
                              liftIO $ createDirectoryIfMissing True roleDir
                              liftIO $ sequence_ [ createSymbolicLink (dirPath </> fn)  (roleDir </> fn) | (_,fn,fc) <- fs' ]
                     )
  -- generate list of links to the files just uploaded
  json $ [ fn | (_, fn, _) <- fs' ]


listCtrl :: ActionM ()
listCtrl = do
  hid'    <- header "X-Hasura-Id"
  hroles' <- header "X-Hasura-Allowed-Roles"

  let hid    = getHid hid'
      hroles = getRoles hroles'

  let dirPath = rootDir </> hid

  files <- liftIO $ listDirectory dirPath

  json $ files
